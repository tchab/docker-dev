#!/usr/bin/env bash

# Debug
set -x

# Fail fast
set -eEo pipefail

# Configuration
UBUNTU_VERSION="22.04"
ASDF_VERSION="0.11.2"
CI_CONTAINER_REGISTRY="${CI_CONTAINER_REGISTRY:-index.docker.io}"
CI_CONTAINER_REPOSITORY="${CI_CONTAINER_REPOSITORY:-devchab}"
# Android SDK version https://developer.android.com/studio/releases/platforms
ANDROID_PLATFORM_VERSION="33"
# Android tools https://developer.android.com/studio/#command-tools
ANDROID_SDK_TOOLS_VERSION="9477386"
# Android build tools https://developer.android.com/studio/releases/build-tools
ANDROID_BUILD_TOOLS_VERSION="33.0.2"
# Android Studio version : to fetch from https://developer.android.com/studio -> Download
#https://redirector.gvt1.com/edgedl/android/studio/ide-zips/2022.1.1.20/android-studio-2022.1.1.20-linux.tar.gz
ANDROID_STUDIO_VERSION="2022.1.1.21"

# JDK Version
JAVA_VERSION="11"

# List of Flutter releases
flutter_releases_json="https://storage.googleapis.com/flutter_infra/releases/releases_linux.json"

# Http proxy shit
http_proxy=${http_proxy:-}
https_proxy=${https_proxy:-}
no_proxy=${no_proxy:-}

# Setup environment variable for JVM / Maven / Gradle proxy setup ...
proxy_setup() {
  local do_proxy=false
  local http_proxy_host
  local http_proxy_port
  local http_proxy_user
  local http_proxy_password
  local https_proxy_host
  local https_proxy_port
  local https_proxy_user
  local https_proxy_password

  # If needed, set up proxy for maven and other java apps ...
  if [ -n "${http_proxy}" ]; then
    # Ugly parsing to fetch needed infos ...
    if grep -q "@" <<<"${http_proxy}"; then
      http_proxy_host=$(echo "${http_proxy}" | sed 's/http:\/\/.*@\(.*\):.*/\1/')
      http_proxy_port=$(echo "${http_proxy}" | sed 's/http:\/\/.*@.*:\(.*\)/\1/' | tr -d "/")
      http_proxy_user=$(echo "${http_proxy}" | sed 's/http:\/\/\(.*\)@.*/\1/' | awk -F: '{print $1}')
      http_proxy_password=$(echo "${http_proxy}" | sed 's/http:\/\/\(.*\)@.*/\1/' | awk -F: '{print $2}')
    else
      http_proxy_host="$(echo "${http_proxy}" | awk -F\[/:\] '{print $4}')"
      http_proxy_port="$(echo "${http_proxy}" | sed -e 's,^.*:,:,g' -e 's,.*:\([0-9]*\).*,\1,g' -e 's,[^0-9],,g')"
    fi
    HTTP_PROXY_HOST=${http_proxy_host}
    HTTP_PROXY_PORT=${http_proxy_port}
    export HTTP_PROXY_HOST HTTP_PROXY_PORT
    do_proxy=true
  fi

  # Same shit for https ...
  if [ -n "${https_proxy}" ]; then
    if grep -q "@" <<<"${https_proxy}"; then
      https_proxy_host=$(echo "${https_proxy}" | sed 's/http:\/\/.*@\(.*\):.*/\1/')
      https_proxy_port=$(echo "${https_proxy}" | sed 's/http:\/\/.*@.*:\(.*\)/\1/' | tr -d "/")
      https_proxy_user=$(echo "${https_proxy}" | sed 's/http:\/\/\(.*\)@.*/\1/' | awk -F: '{print $1}')
      https_proxy_password=$(echo "${https_proxy}" | sed 's/http:\/\/\(.*\)@.*/\1/' | awk -F: '{print $2}')
    else
      https_proxy_host="$(echo "${https_proxy}" | awk -F\[/:\] '{print $4}')"
      https_proxy_port="$(echo "${https_proxy}" | sed -e 's,^.*:,:,g' -e 's,.*:\([0-9]*\).*,\1,g' -e 's,[^0-9],,g')"
    fi
    HTTPS_PROXY_HOST=${https_proxy_host}
    HTTPS_PROXY_PORT=${https_proxy_port}
    export HTTPS_PROXY_HOST HTTPS_PROXY_PORT
    do_proxy=true
  fi

  # According to https://docs.oracle.com/javase/8/docs/api/java/net/doc-files/net-properties.html
  # nonProxyHosts is the same for http and https
  [ -n "${no_proxy}" ] && local proxy_avoid="${no_proxy//,/\\|}"
  [ -n "${proxy_avoid}" ] && local java_no_proxy="-Dhttp.nonProxyHosts=${proxy_avoid}"

  [ -n "${http_proxy_host}" ] && local java_proxy_http="-Dhttp.proxyHost=${http_proxy_host} -Dhttp.proxyPort=${http_proxy_port}"
  [ -n "${http_proxy_user}" ] && local java_proxy_http_credentials="-Dhttp.proxyUser=${http_proxy_user} -Dhttp.proxyPassword=${http_proxy_password}"
  [ -n "${https_proxy_host}" ] && local java_proxy_https="-Dhttps.proxyHost=${https_proxy_host} -Dhttps.proxyPort=${https_proxy_port}"
  [ -n "${https_proxy_user}" ] && local java_proxy_https_credentials="-Dhttps.proxyUser=${https_proxy_user} -Dhttps.proxyPassword=${https_proxy_password}"

  if [[ "${do_proxy}" == true ]]; then
    local java_full_proxy_conf="${java_proxy_http} ${java_proxy_http_credentials} ${java_proxy_https} ${java_proxy_https_credentials} ${java_no_proxy}"
    JAVA_OPTS="${java_full_proxy_conf} ${JAVA_OPTS}"
    MAVEN_OPTS="${java_full_proxy_conf} ${MAVEN_OPTS}"
    GRADLE_OPTS="${java_full_proxy_conf} ${GRADLE_OPTS}"
    HTTP_PROXY=${http_proxy}
    HTTPS_PROXY=${https_proxy}
    NO_PROXY=${no_proxy}
    ANDROID_SDK_MANAGER_PROXY_OPTS="--proxy=http --proxy_host=${HTTP_PROXY_HOST} --proxy_port=${HTTP_PROXY_PORT}"
    export HTTP_PROXY HTTPS_PROXY NO_PROXY JAVA_OPTS MAVEN_OPTS GRADLE_OPTS ANDROID_SDK_MANAGER_PROXY_OPTS
  fi
}

# Get list of latest Flutter versions for past period
#last_year=$(date --date="1 year ago" +%Y-%m-%d)
last_month=$(date --date="1 month ago" +%Y-%m-%d)
#flutter_versions=$(curl -s ${flutter_releases_json} |
#     jq -r '.releases[] |select(.channel=="stable" and .release_date >= "'"${last_month}"'") |.version' | sort -V |tail -3)
# or force a specific Flutter version
flutter_versions="3.3.8"

flutter_images=""
for version in ${flutter_versions}; do
  flutter_images="${flutter_images} flutter-ci:${version} flutter:${version}"
done

# Http proxy shit
http_proxy=${http_proxy:-}
https_proxy=${https_proxy:-}
no_proxy=${no_proxy:-}
proxy_setup

# Order matters !
DOCKER_IMAGES="ubuntu-base:${UBUNTU_VERSION} java-ci:${JAVA_VERSION} android-sdk-ci:${ANDROID_PLATFORM_VERSION} ubuntu:${UBUNTU_VERSION} android-sdk:${ANDROID_PLATFORM_VERSION} android-studio:${ANDROID_STUDIO_VERSION}${flutter_images}"

for img in ${DOCKER_IMAGES}; do
  img_name=$(echo "${img}" | cut -d':' -f1)
  img_version=$(echo "${img}" | cut -d':' -f2)
  img_version_tag=$(echo "${img_version}" | tr '+' '-')
  echo "Building version ${img_version} for image ${img_name}"

  # Build image with specified versions
  docker build --network host \
    -t "${CI_CONTAINER_REPOSITORY}/${img_name}:${img_version_tag}" \
    --build-arg http_proxy="${http_proxy}" \
    --build-arg https_proxy="${https_proxy}" \
    --build-arg no_proxy="${no_proxy}" \
    --build-arg HTTP_PROXY="${HTTP_PROXY}" \
    --build-arg HTTPS_PROXY="${HTTPS_PROXY}" \
    --build-arg NO_PROXY="${NO_PROXY}" \
    --build-arg HTTP_PROXY_HOST="${HTTP_PROXY_HOST}" \
    --build-arg HTTP_PROXY_PORT="${HTTP_PROXY_PORT}" \
    --build-arg HTTPS_PROXY_HOST="${HTTPS_PROXY_HOST}" \
    --build-arg HTTPS_PROXY_PORT="${HTTPS_PROXY_PORT}" \
    --build-arg ANDROID_SDK_MANAGER_PROXY_OPTS="${ANDROID_SDK_MANAGER_PROXY_OPTS}" \
    --build-arg JAVA_OPTS="${JAVA_OPTS}" \
    --build-arg MAVEN_OPTS="${MAVEN_OPTS}" \
    --build-arg GRADLE_OPTS="${GRADLE_OPTS}" \
    --build-arg CI_CONTAINER_REGISTRY="${CI_CONTAINER_REGISTRY}" \
    --build-arg CI_CONTAINER_REPOSITORY="${CI_CONTAINER_REPOSITORY}" \
    --build-arg UBUNTU_VERSION="${UBUNTU_VERSION}" \
    --build-arg ASDF_VERSION="${ASDF_VERSION}" \
    --build-arg JAVA_VERSION="${JAVA_VERSION}" \
    --build-arg ANDROID_PLATFORM_VERSION="${ANDROID_PLATFORM_VERSION}" \
    --build-arg ANDROID_BUILD_TOOLS_VERSION="${ANDROID_BUILD_TOOLS_VERSION}" \
    --build-arg ANDROID_SDK_TOOLS_VERSION="${ANDROID_SDK_TOOLS_VERSION}" \
    --build-arg ANDROID_STUDIO_BUILD="${ANDROID_STUDIO_BUILD}" \
    --build-arg ANDROID_STUDIO_VERSION="${ANDROID_STUDIO_VERSION}" \
    --build-arg FLUTTER_VERSION="${img_version}" \
    -f "${img_name}"/Dockerfile "${img_name}"
  # Tag and push image to Docker Repo
  if [[ -n "${CI_CONTAINER_REGISTRY_PASSWORD}" ]]; then
    echo "${CI_CONTAINER_REGISTRY_PASSWORD}" | docker login -u "${CI_CONTAINER_REPOSITORY}" --password-stdin "${CI_CONTAINER_REGISTRY}"
  fi
  if [[ ${CI_CONTAINER_REGISTRY} == *"docker.io"* ]]; then
    docker push "${CI_CONTAINER_REPOSITORY}/${img_name}:${img_version_tag}"
  else
    docker tag "${CI_CONTAINER_REPOSITORY}/${img_name}:${img_version_tag}" "${CI_CONTAINER_REGISTRY}/${CI_CONTAINER_REPOSITORY}/${img_name}:${img_version_tag}"
    docker push "${CI_CONTAINER_REGISTRY}/${CI_CONTAINER_REPOSITORY}/${img_name}:${img_version_tag}"
  fi
done
