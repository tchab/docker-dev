# docker-dev

Ubuntu based Docker images for multiple development purpose.

# Usage

- Pull image from docker hub

```sh
docker pull devchab/[IMAGE_NAME]
```

- Launch image (example with flutter image, don't forget to replace */path/to/your/project/sources* with the real path)

```sh
PREFS_ROOT=".devchab"
LOCAL_PREFS="${HOME}/${PREFS_ROOT}"
LOCAL_GIT_CONFIG="${HOME}/.gitconfig"
mkdir -p "${LOCAL_PREFS}/system-images"
docker run -it \
       --rm \
       --name flutter \
       -e DISPLAY="${DISPLAY}" \
       -e GRADLE_OPTS="${GRADLE_OPTS}" \
       -h localhost \
       -v "${LOCAL_GIT_CONFIG}":/home/dev/.gitconfig \
       -v "${LOCAL_PREFS}":/home/dev \
       -v "${LOCAL_PREFS}"/system-images:/opt/android-sdk-linux/system-images \
       -v "/path/to/your/project/sources":/src \
       -v /tmp/.X11-unix:/tmp/.X11-unix \
       -v /etc/localtime:/etc/localtime:ro \
       -v /usr/share/icons:/usr/share/icons:ro \
       -v /usr/share/zoneinfo:/usr/share/zoneinfo:ro \
       -v /dev/kvm:/dev/kvm \
       -w /src \
       --privileged \
       devchab/flutter \
       /opt/android-studio/bin/studio.sh

```
